package com.example.seventhhomework

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.seventhhomework.databinding.ItemLayoutBinding

class RecyclerViewAdapter(private val items: MutableList<ItemModel>): RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView = ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ItemViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindNewInfo()
    }

    override fun getItemCount() = items.size

    inner class ItemViewHolder(private val binding: ItemLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var itemFromList: ItemModel
        fun bindNewInfo(){
            itemFromList = items[adapterPosition]
            binding.image.setImageResource(itemFromList.image)
            binding.title.text = itemFromList.title
            binding.description.text = itemFromList.description
        }
    }
}