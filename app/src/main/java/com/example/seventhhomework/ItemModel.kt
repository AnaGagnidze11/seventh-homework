package com.example.seventhhomework

data class ItemModel (val image: Int, val title: String, val description: String)