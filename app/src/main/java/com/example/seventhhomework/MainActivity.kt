package com.example.seventhhomework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.seventhhomework.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val items = mutableListOf<ItemModel>()

    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setTheme(R.style.Theme_SeventhHomework)
        setContentView(binding.root)
        addItems()
        init()
    }

    private fun init(){
        adapter = RecyclerViewAdapter(items)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
    }

    private fun addItems(){
        items.add(ItemModel(R.mipmap.ic_apricot, "Apricot", "Great source of many antioxidants"))
        items.add(ItemModel(R.mipmap.ic_avocado, "Avocado", "Great source of vitamins C, E, K"))
        items.add(ItemModel(R.mipmap.ic_bananas, "Bananas", "Good for your skin"))
        items.add(ItemModel(R.mipmap.ic_blueberry, "Blueberry", "The King of Antioxidant Foods"))
        items.add(ItemModel(R.mipmap.ic_cucumber, "Cucumber", "Promotes Hydration"))
        items.add(ItemModel(R.mipmap.ic_lemon, "Lemon", "Good source of vitamin C"))
        items.add(ItemModel(R.mipmap.ic_strawberry, "Strawberry", "Great addition to a weight loss diet"))
        items.add(ItemModel(R.mipmap.ic_tomato, "Tomato", "Supports heart health"))
        items.add(ItemModel(R.mipmap.ic_watermelon, "Watermelon", "May Help Prevent Cancer"))
        items.add(ItemModel(R.mipmap.ic_apple, "Apple", "Can Support a Healthy Immune System"))
        items.add(ItemModel(R.mipmap.ic_orange, "Orange", "Protects your cells from damage"))
        items.add(ItemModel(R.mipmap.ic_grapes, "Grapes", "May Promote Appetite Control"))
    }

}

